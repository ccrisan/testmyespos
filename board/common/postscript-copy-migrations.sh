#!/bin/sh

# Manually copy ".py" migrations, since what we get by default are ".pyc" files which cannot be used for migration
cp ${BUILD_DIR}/python-testmyesp-*/testmyesp/migrations/*.py ${TARGET_DIR}/usr/lib/python3.7/site-packages/testmyesp/migrations
