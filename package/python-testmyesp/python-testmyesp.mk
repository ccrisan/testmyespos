################################################################################
#
# python-testmyesp
#
################################################################################

PYTHON_TESTMYESP_VERSION = 0.6.7
PYTHON_TESTMYESP_SOURCE = testmyesp-$(PYTHON_TESTMYESP_VERSION).tar.gz
PYTHON_TESTMYESP_SITE = https://pypi.org/packages/source/t/testmyesp
PYTHON_TESTMYESP_SETUP_TYPE = setuptools
PYTHON_TESTMYESP_LICENSE = BSD
PYTHON_TESTMYESP_LICENSE_FILES = LICENSE.txt
PYTHON_TESTMYESP_DEPENDENCIES = host-python-fastentrypoints

$(eval $(python-package))
